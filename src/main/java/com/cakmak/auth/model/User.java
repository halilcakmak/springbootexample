package com.cakmak.auth.model;

import com.google.common.collect.Sets;
import lombok.*;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "USERS", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "username"
        }),
        @UniqueConstraint(columnNames = {
                "email"
        })
})
@EntityListeners(AuditingEntityListener.class)
public class User implements Serializable, UserDetails {

    private static final long serialVersionUID = 2427238057150579366L;

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Size(min=3, max = 50)
    private String name;

    @Size(min=3, max = 50)
    private String username;

    @NaturalId
    @Size(max = 50)
    private String email;

    @CreatedDate
    private Date registered;

    private Boolean enabled;

    @Size(min=6, max = 100)
    private String password;

    private String token;


    @ElementCollection(targetClass = RoleName.class, fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "USER_ROLES")
    @Column(name = "user_roles")
    Set<RoleName> roleNames = new HashSet<>(Sets.newHashSet(RoleName.ROLE_USER));

    public User(String name,String username,String email,String password){
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public boolean hasRoles(RoleName... roles) {

        for (RoleName role : roles) {
            if (!roleNames.contains(role)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        if (hasRoles(RoleName.ROLE_USER)) {
            grantedAuthorities.add(new SimpleGrantedAuthority(RoleName.ROLE_USER.name()));
        }

        if (hasRoles(RoleName.ROLE_ADMIN)) {
            grantedAuthorities.add(new SimpleGrantedAuthority(RoleName.ROLE_ADMIN.name()));
        }
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return enabled;
    }

    @Override
    public boolean isAccountNonLocked() {
        return enabled;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return enabled;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}

