package com.cakmak.auth.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_TEST,
    ROLE_ADMIN
}