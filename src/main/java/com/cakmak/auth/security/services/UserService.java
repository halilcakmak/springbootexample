package com.cakmak.auth.security.services;


import com.cakmak.auth.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User register(User user);

    User findByUsername(String username);

}
